jQuery(document).ready(function ($) {

    'use strict';

    /************** Mixitup (Filter Projects) *********************/
    $('.projects-holder').mixitup({
        effects: ['fade', 'grayscale'],
        easing: 'snap',
        transitionSpeed: 400
    });

    /**Acitve menu button**/
    $('a').click(function () {
        $('.active').removeClass('active');
        $(this).addClass('active');
    });
    /***Responsive nav-bar***/
    $(document).ready(function () {
        var stickyNavTop = $('header').offset().top;

        var stickyNav = function () {
            var scrollTop = $(window).scrollTop();

            if (scrollTop > stickyNavTop) {
                $('header').addClass('sticky');
            } else {
                $('header').removeClass('sticky');
            }
        };

        stickyNav();

        $(window).scroll(function () {
            stickyNav();
        });
    });

    /**Modal video.js player**/
    $('.slider-video').on('click', function () {
        var videoTag = document.getElementById($(this).attr("id"));
        var videoSrc = videoTag.getElementsByTagName('source')[0].outerHTML;
        var div = document.getElementById('custom');
        var closeButton = document.createElement('button');
        closeButton.id = 'close-btn-id';
        div.style.display = 'flex';
        var playerCheck = document.getElementById('my-player');
        if (!playerCheck) {
            var myPlayer = document.createElement('video');
            myPlayer.id = 'my-player';
            myPlayer.className = 'video-js';
            div.appendChild(myPlayer);
            myPlayer.innerHTML = videoSrc;
            var player = videojs(myPlayer.id, {controls: true, preload: 'auto', fluid: true}, function onPlayerReady() {
                this.play();
            });
        }
    });
    $('.close-btn').on('click', function () {
        var div = document.getElementById('custom');
        var myPlayer = videojs("my-player");
        myPlayer.dispose();
        div.style.display = 'none';
    });
});

// Cache selectors
var topMenu = $("#nav-bar"),
    topMenuHeight = topMenu.outerHeight() + 450,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function () {
        var item = $($(this).attr("href"));
        if (item.length) {
            return item;
        }
    });

// Bind to scroll
$(window).scroll(function () {
    // Get container scroll position
    var fromTop = $(this).scrollTop() + topMenuHeight;

    // Get id of current scroll item
    var cur = scrollItems.map(function () {
        if ($(this).offset().top < fromTop)
            return this;
    });
    // Get the id of the current element
    cur = cur[cur.length - 1];
    var id = cur && cur.length ? cur[0].id : "";
    // Set/remove active class
    //var a = menuItems.filter("[href='#" + id + "']");
    document.querySelector('.active').setAttribute('class', ' ');
    menuItems
        .filter("[href='#" + id + "']").addClass("active");

    switch (id){
        case 'first':
            $('#1').addClass('animated bounceInDown');
            $('#2').addClass('animated rubberBand');
            break;
        case 'second':
            $('#3').addClass('animated bounceInUp');
            $('.service-item').addClass('animated tada');
            break;
        case 'third':
            $('.left-image').addClass('animated rotateIn');
            $('.right-text').addClass('animated slideInRight');
            break;
        case 'fourth':
            $('#4').addClass('animated zoomInRight');
            $('.slider-video').addClass('animated zoomInRight');
            break;
        case 'fivth':
            $('#5').addClass('animated flipInX');
            $('#6').addClass('animated rotateInUpLeft');
            $('.col-lg-2').addClass('animated rotateInUpRight');
            break;
        case 'sixth':
            $('#contact').addClass('animated bounceInRight');
            $('.right-info').addClass('animated bounceInLeft');
            $('#foot').addClass('animated tada');
            break;
    }
});

/****Animate.css****/
$('#1').addClass('animated bounceInDown');
$('#2').addClass('animated rubberBand');












