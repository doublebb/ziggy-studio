/**
 * Created by tech-support on 31.10.2017.
 */
var swiper = new Swiper('.swiper-container', {
        zoom: true,
        slidesPerView: 3,
        freeMode: false,
        pagination:false,
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 30,
        autoplay: false,
        autoplayDisableOnInteraction: true
    }
);

